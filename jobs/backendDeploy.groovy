pipelineJob('backendDeploy') {

    description("Pipeline flow for creating and deploying meetlang backend application to server")
    parameters{
        stringParam("GIT_BRANCH", "main", "Branch from which the application should be built, main is the default")
    }
    definition {
        cps{
            script(readFileFromWorkspace('common/backendDeploy.groovy'))
        }
    }
}
