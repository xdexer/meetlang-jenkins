job('seed') {
triggers {
    scm('H/5 * * * *')
}  
scm { 
    git ('https://gitlab.com/xdexer/meetlang-jenkins.git')
  }
  steps {
    dsl {
      external('jobs/*.groovy')  
      removeAction('DELETE')
    }
  }
}
