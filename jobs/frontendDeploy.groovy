pipelineJob('frontendDeploy') {

    description("Pipeline flow for creating and deploying meetlang frontend application to server")
    parameters{
        stringParam("GIT_BRANCH", "main", "Branch from which the application should be built, main is the default")
    }
    definition {
        cps{
            script(readFileFromWorkspace('common/frontendDeploy.groovy'))
        }
    }
}
