def envCleanup(build_number){
    println('Cleaning dangling containers, images, volumes')
    sh '''
    rm backend_package.tar
    docker rmi $(docker images 'meetlang-backend*' -a -q)
    '''
    println('Clean-up completed')
}

node {
    stage ('Prepare environment') {
        println("Downloading main branch of backend repository")
        git branch: "${env.GIT_BRANCH}", url: 'https://gitlab.com/xdexer/meetlang-backend.git'
        println("Repository download complete")
    }

    stage ('Build docker image') {
        println("Building the application")
        def app = docker.build("meetlang-backend:${env.BUILD_ID}")
        sh '''
          ls -al
          pwd
        '''
    }
    
    stage('Pytest') {
        println("Running python unit tests")
        DOCKER_EXIT_CODE = sh(
            script: "docker run --rm meetlang-backend:${env.BUILD_ID} pytest",
            returnStatus: true
        )
        println("DOCKER EXIT CODE: ${DOCKER_EXIT_CODE}")
        if("${DOCKER_EXIT_CODE}" != "0"){
            sh "docker logs ${containerID}"
            envCleanup("${env.BUILD_ID}")
            error("BUILD FAILED WHEN UNIT TESTING")
        }
    }

    stage ('Deploy') {
        println("Sending build to the production server")
        sh '''
          docker save -o backend_package.tar meetlang-backend
          ls -al
          pwd
        '''
        sshPublisher(
            publishers: 
            [
                sshPublisherDesc(configName: 'meetlang-server', transfers: 
                [sshTransfer(
                    cleanRemote: false, 
                    excludes: '', 
                    execCommand: './backend-deploy/run_container.sh',
                    execTimeout: 120000, 
                    flatten: false, 
                    makeEmptyDirs: false, 
                    noDefaultExcludes: false, 
                    patternSeparator: '[, ]+', 
                    remoteDirectory: '/backend-deploy', 
                    remoteDirectorySDF: false, 
                    removePrefix: '', 
                    sourceFiles: '') 
                ], 
                usePromotionTimestamp: false, 
                useWorkspaceInPromotion: false, 
                verbose: false
                )
            ]
        )
    }

    stage ('Clean-up') {
        envCleanup("${env.BUILD_ID}")
    }
}