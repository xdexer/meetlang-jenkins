def envCleanup(build_number){
    println('Cleaning dangling containers, images, volumes')
    sh '''
    rm frontend_package.tar
    docker rmi --force $(docker images 'meetlang-frontend*' -a -q)
    docker image prune --force
    '''
    println('Clean-up completed')
}

node {
    stage ('Prepare environment') {
        println("Downloading main branch of frontend repository")
        git branch: "${env.GIT_BRANCH}", url: 'https://gitlab.com/xdexer/meetlang-frontend.git'
        println("Repository download complete")
    }

    stage ('Build docker image') {
        println("Building the application")
        DOCKER_EXIT_CODE = sh(
            script: "docker build -t meetlang-frontend:${env.BUILD_ID} ./meetlang/",
            returnStatus: true
        )
        println("DOCKER EXIT CODE: ${DOCKER_EXIT_CODE}")
        if("${DOCKER_EXIT_CODE}" != "0"){
            sh "docker logs ${containerID}"
            envCleanup("${env.BUILD_ID}")
            error("BUILD FAILED WHEN BUILDING")
        }
        sh '''
          ls -al
          pwd
        '''
    }

    stage ('Deploy') {
        println("Sending build to the production server")
        sh '''
          docker save -o frontend_package.tar meetlang-frontend
          ls -al
          pwd
        '''
        sshPublisher(
            publishers: 
            [
                sshPublisherDesc(configName: 'meetlang-server', transfers: 
                [sshTransfer(
                    cleanRemote: false, 
                    excludes: '', 
                    execCommand: './frontend-deploy/run_container.sh',
                    execTimeout: 120000, 
                    flatten: false, 
                    makeEmptyDirs: false, 
                    noDefaultExcludes: false, 
                    patternSeparator: '[, ]+', 
                    remoteDirectory: '/frontend-deploy', 
                    remoteDirectorySDF: false, 
                    removePrefix: '', 
                    sourceFiles: '') 
                ], 
                usePromotionTimestamp: false, 
                useWorkspaceInPromotion: false, 
                verbose: false
                )
            ]
        )
    }

    stage ('Clean-up') {
        envCleanup("${env.BUILD_ID}")
    }
}