#meetlang-jenkins
Repository for jenkins job definitions and utility scripts to automate processes needed in application development. Every .groovy script defined in jobs folder is generated in meetlang-jenkins by the seed.groovy job.
